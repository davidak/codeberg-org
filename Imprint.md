
Codeberg is a non-profit organisation dedicated to build and maintain supporting infrastructure for the creation, collection, dissemination, and archiving of Free and Open Source Software. If you have questions, suggestions or comments, please do not hesitate to contact us at [contact@codeberg.org](mailto:contact@codeberg.org).

## Impressum nach §5 TMG (Imprint according to German Law)

```text
Codeberg e.V.
Gormannstraße 14
10119 Berlin

E-Mail: contact@codeberg.org

Geschäftsführender Vorstand: Holger Waechtler
Eingetragen im Vereinsregister des Amtsgerichts Charlottenburg VR36929.
```

## SSH Fingerprints

Below you find the fingerprints for SSH hostkey verification:

```
# codeberg.org:22 SSH-2.0-OpenSSH_7.9p1 Debian-10+deb10u1
codeberg.org ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBL2pDxWr18SoiDJCGZ5LmxPygTlPu+cCKSkpqkvCyQzl5xmIMeKNdfdBpfbCGDPoZQghePzFZkKJNR/v9Win3Sc=
# codeberg.org:22 SSH-2.0-OpenSSH_7.9p1 Debian-10+deb10u1
codeberg.org ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC8hZi7K1/2E2uBX8gwPRJAHvRAob+3Sn+y2hxiEhN0buv1igjYFTgFO2qQD8vLfU/HT/P/rqvEeTvaDfY1y/vcvQ8+YuUYyTwE2UaVU5aJv89y6PEZBYycaJCPdGIfZlLMmjilh/Sk8IWSEK6dQr+g686lu5cSWrFW60ixWpHpEVB26eRWin3lKYWSQGMwwKv4LwmW3ouqqs4Z4vsqRFqXJ/eCi3yhpT+nOjljXvZKiYTpYajqUC48IHAxTWugrKe1vXWOPxVXXMQEPsaIRc2hpK+v1LmfB7GnEGvF1UAKnEZbUuiD9PBEeD5a1MZQIzcoPWCrTxipEpuXQ5Tni4mN
# codeberg.org:22 SSH-2.0-OpenSSH_7.9p1 Debian-10+deb10u1
codeberg.org ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIVIC02vnjFyL+I4RHfvIGNtOgJMe769VTF1VR4EB3ZB
```
